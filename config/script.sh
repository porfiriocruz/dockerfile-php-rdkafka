#!/bin/bash
# librdkafka
cd /tmp && git clone https://github.com/edenhill/librdkafka.git &&
cd /tmp/librdkafka && ./configure && make && make install && rm -rf /tmp/librdkafka

# php-rdkafka
pecl install rdkafka &&
echo "extension=rdkafka.so" > /etc/php5/apache2/conf.d/rdkafka.ini  