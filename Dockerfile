FROM ubuntu:14.04
MAINTAINER Porfiro Cruz de Jesus <porfirio.cruz@enova.mx>
RUN apt-get update \
    && apt-get install -y \
    curl  \
    cron  \
    git   \
    unzip \
    vim   \
    nano  \
    apache2 \
    php5 \
    php5-cli \
    libapache2-mod-php5 \
    php5-gd \
    php5-json \
    php5-ldap \
    php5-mysql \
    php5-pgsql \
    php5-redis \
	php5-common \
	php5-mcrypt \
	php5-memcache \
	php5-sqlite \
	php5-xsl \
	php5-mcrypt \
    gcc g++     \
    php5-dev autoconf \
    libpcre3-dev make python \
	&& apt-get clean && apt-get autoclean && apt-get autoremove \
	&& rm -rf /var/lib/apt/lists/*
	
RUN apt-get install net-tools \
	-y iputils-ping

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
	
RUN a2enmod rewrite

RUN export MAKO_PMC_ENV=local_pmc_dev

ENV TZ=America/Mexico_City
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD config/php/php.ini /etc/php5/apache2/php.ini
ADD config/apache/sites-available/mako.conf /etc/apache2/sites-available/000-default.conf

RUN mkdir -p /var/www/Mako

WORKDIR /var/www/Mako

COPY config/script.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/script.sh && chown -R www-data:www-data /var/www/Mako
RUN /usr/local/bin/script.sh

EXPOSE 80
CMD ["apache2ctl", "-D","FOREGROUND"]
