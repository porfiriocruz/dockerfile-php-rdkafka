instalación de imagen y contenedor

git clone git@bitbucket.org:porfiriocruz/dockerfile-php-rdkafka.git

cd dockerfile-php-rdkafka

Crear imagen:

docker build  -t mako/ubuntu:latest .

Ejemplo de como crear un contenedor con volumen, de  nuestro código fuente de Mako:

docker run --name local.mako.ria -v ~/Documentos/Porfirio/contenedores/RIA/mako_local/mako/Mako:/var/www/Mako  -p 9201:80 -d mako/ubuntu
